//
//  CodableModels.swift
//  Foodjin
//
//  Created by Navpreet Singh on 24/05/19.
//  Copyright © 2019 Foodjin. All rights reserved.
//

import Foundation
import Default

// MARK: - Response Status
struct ResponseStatus: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
    }
}

// MARK: - Version
struct Version: Codable {
    var StatusCode: Int?
    var ErrorMessageTitle, ErrorMessage: String?
    var Status: Bool?
    var ApiVersion, ApiKey: String?
    var TokenDispStatus, IsBaseSplashNeeded, IsMultiVendorSupported: Bool?
    var SplashURL: String?
    var IsTutorialNeeded: Bool?
    var TutorialVersion: String?
    var TutorialCount: Int?
    var IsMultiLanguage: Bool?
    var LanguageCount: Int?
    var DataBaseVersion: String?
    var IsDispGoogMap: Bool?
    var DispGoogMapURL: String?
    var IsSocketAvailable: Bool?
    var SocketURL, SocketPort: String?
    var IsDelTakeAwayOnDashboard: Bool?
    var ApiVerLangs: [APIVerLang]?
    var TutorialURLs: [JSONAny]?
    var StoreID: Int?
    var StoreCurrency: String?
    var GetHelp: String?
    var Privacy: String?
    var TermsCondition: String?
    var SupportChatUrl: String?
    
    
    
    enum CodingKeys: String, CodingKey {
        case StatusCode = "statusCode"
        case GetHelp = "getHelp"
        case Privacy = "privacy"
        case TermsCondition = "termsCondition"
        case SupportChatUrl = "supportChatUrl"
        case ErrorMessageTitle = "errorMessageTitle"
        case ErrorMessage = "errorMessage"
        case Status = "status"
        case ApiVersion = "apiVersion"
        case ApiKey = "apiKey"
        case TokenDispStatus = "tokenDispStatus"
        case IsBaseSplashNeeded = "isBaseSplashNeeded"
        case IsMultiVendorSupported = "isMultiVendorSupported"
        case SplashURL = "splashURL"
        case IsTutorialNeeded = "isTutorialNeeded"
        case TutorialVersion = "tutorialVersion"
        case TutorialCount = "tutorialCount"
        case IsMultiLanguage = "isMultiLanguage"
        case LanguageCount = "languageCount"
        case DataBaseVersion = "dataBaseVersion"
        case IsDispGoogMap = "isDispGoogMap"
        case DispGoogMapURL = "dispGoogMapURL"
        case IsSocketAvailable = "isSocketAvailable"
        case SocketURL = "socketURL"
        case SocketPort = "socketPort"
        case IsDelTakeAwayOnDashboard = "isDelTakeAwayOnDashboard"
        case ApiVerLangs = "apiVerLangs"
        case TutorialURLs = "tutorialURLs"
        case StoreID = "storeId"
        case StoreCurrency = "storeCurrency"
    }
}

// MARK: - APIVerLang
struct APIVerLang: Codable {
    var apiVerLangID: Int?
    var apiVerLang: String?
    
    enum CodingKeys: String, CodingKey {
        case apiVerLangID = "apiVerLangId"
        case apiVerLang = "apiVerLang"
    }
}

// MARK: - Register
struct Register: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: ResponseObj?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct ResponseObj: Codable {
    var userID: Int?
    var firstName, lastName: String?
    var gender: String?
    var userStatus: String?
    var isFCMToken: Bool?
    var deviceToken: String?
    var otp: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "userID"
        case firstName = "firstName"
        case lastName = "lastName"
        case gender = "gender"
        case userStatus = "userStatus"
        case isFCMToken = "isFCMToken"
        case deviceToken = "deviceToken"
        case otp = "otp"
    }
}

// MARK: - VerifyRegistration
struct VerifyRegistration: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: Int?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResetPassword
struct ResetPassword: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: Int?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResendOtpResponse
struct ResendOtpResponse: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - Profile
struct Profile: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: ResponseObject?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct ResponseObject: Codable {
    var id: Int?
    var firstName, lastName, email: String?
    var mobileStd: Int?
    var mobileNo: String?
    var profilePic: String?
    var storeID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "firstName"
        case lastName = "lastName"
        case email = "email"
        case mobileStd = "mobileStd"
        case mobileNo = "mobileNo"
        case profilePic = "profilePic"
        case storeID = "storeId"
    }
}

// MARK: - AddressResponse
struct AddressResponse: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [AddressArray]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - AddressArray
struct AddressArray: Codable {
    var id: Int?
    var label, addressLine1, addressLine2, zipCode: String?
    var city: String?
    var latPos, longPos: Double?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case addressLine1 = "addressLine1"
        case addressLine2 = "addressLine2"
        case zipCode = "zipCode"
        case city = "city"
        case latPos = "latPos"
        case longPos = "longPos"
    }
}

// MARK: - BlogPosts
struct BlogPosts: Codable {
    var statusCode: Int?
    var errorMessageTitle: JSONNull?
    var errorMessage: String?
    var status: Bool?
    var responseObj: [BlogPost]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct BlogPost: Codable {
    var blogID: Int?
    var blogTitle, blogDescription: String?
    var allowComments: Bool?
    var blogTags, blogStartDate, blogEndDate, blogCreatedOn: String?
    var blogCommentsCount: Int?
    let customerImages: [CustomerImage]?
    var blogImage: String?
    var chefImage, blogTime: String?
    
    enum CodingKeys: String, CodingKey {
        case blogID = "blogId"
        case blogTitle = "blogTitle"
        case blogDescription = "blogDescription"
        case allowComments = "allowComments"
        case blogTags = "blogTags"
        case blogStartDate = "blogStartDate"
        case blogEndDate = "blogEndDate"
        case blogCreatedOn = "blogCreatedOn"
        case blogCommentsCount = "blogCommentsCount"
        case customerImages = "customerImages"
        case blogImage = "blogImage"
        case chefImage = "chefImage"
        case blogTime = "blogTime"
    }
}



// MARK: - BlogPostsDetail
struct BlogPostsDetail: Codable {
    let statusCode: Int?
    let errorMessageTitle: String?
    let errorMessage: String?
    let status: Bool?
    let responseObj: BlogPostsDetailResponse?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct BlogPostsDetailResponse: Codable {
    let blogID: Int?
    let blogTitle, blogDescription: String?
    let allowComments: Bool?
    let blogTags, blogStartDate, blogEndDate: String?
    let blogCreatedOn: String?
    let blogCommentsCount: Int?
    let blogImage: String?
    let chefImage: String?
    let blogTime: String?
    let shareInstaLink, shareFbLink, sharePinterstLink: String?
    let blogComments: [BlogComment]?
    
    enum CodingKeys: String, CodingKey {
        case blogID = "blogId"
        case blogTitle = "blogTitle"
        case blogDescription = "blogDescription"
        case allowComments = "allowComments"
        case blogTags = "blogTags"
        case blogStartDate = "blogStartDate"
        case blogEndDate = "blogEndDate"
        case blogCreatedOn = "blogCreatedOn"
        case blogCommentsCount = "blogCommentsCount"
        case blogImage = "blogImage"
        case chefImage = "chefImage"
        case blogTime = "blogTime"
        case shareInstaLink = "shareInstaLink"
        case shareFbLink = "shareFbLink"
        case sharePinterstLink = "sharePinterstLink"
        case blogComments = "blogComments"
    }
}

// MARK: - BlogComment
struct BlogComment: Codable {
    let userImage: String?
    let userName, comment: String?
    
    enum CodingKeys: String, CodingKey {
        case userImage = "userImage"
        case userName = "userName"
        case comment = "comment"
    }
}


// MARK: - OrderHistory
struct OrderHistory: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: JSONNull?
    var status: Bool?
    var responseObj: [Order]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct Order: Codable {
    var orderID: Int?
    var orderNumber: String?
    var orderStatus: Int?
    var status, orderTime, deliveredTime: String?
    var isOpenForReview: Bool?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case orderNumber = "orderNumber"
        case orderStatus = "orderStatus"
        case status = "status"
        case orderTime = "orderTime"
        case deliveredTime = "deliveredTime"
        case isOpenForReview = "isOpenForReview"
    }
}

// MARK: - OrderReviewData
struct OrderReviewData: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: JSONNull?
    var status: Bool?
    var responseObj: [OrderReviewDetails]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

struct OrderStatusData: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: JSONNull?
    var status: Bool?
    var responseObj: [StatusData]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

struct Object: Codable {
    
}

struct StatusData :Codable {
    var status: String?
    var statusId: Int?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case statusId = "statusId"
       
    }
}




struct MerchantReviewDetails: Codable {
    var merchantId: Int
    var picture: String?
    var pictureId: Int?
    var reviewOptionsS: String?
    var subtitle: String?
    var title: String?
    var reviewOptions: [String]?
    
    enum CodingKeys: String, CodingKey {
        case merchantId = "merchantId"
        case picture = "picture"
        case pictureId = "pictureId"
        case reviewOptionsS = "reviewOptionsS"
        case subtitle = "subtitle"
        case title = "title"
        case reviewOptions = "reviewOptions"
    }
    
}

// MARK: - ResponseObj
struct OrderReviewDetails: Codable {
    var orderID: Int?
    var orderNumber: String?
    var products: [Product]?
    var merchantReview: MerchantReviewDetails?
    var agentReview: AgentReviewDetails?
    var isAgentReview: Bool?
    var isItemReview: Bool?
    var isMerchantReview: Bool?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case orderNumber = "orderNumber"
        case products = "products"
        case merchantReview = "merchantReview"
        case agentReview = "agentReview"
        case isAgentReview = "isAgentReview"
        case isItemReview = "isItemReview"
        case isMerchantReview = "isMerchantReview"
    }
}

struct AgentReviewDetails: Codable {
    var agentId: Int?
    var picture: String?
    var pictureId: Int?
    var reviewOptionsS: String?
    var subtitle: String?
    var title: String?
    var reviewOptions: [String]?
    
    enum CodingKeys: String, CodingKey {
        case agentId = "agentId"
        case picture = "picture"
        case pictureId = "pictureId"
        case reviewOptionsS = "reviewOptionsS"
        case subtitle = "subtitle"
        case title = "title"
        case reviewOptions = "reviewOptions"
    }
    
}

// MARK: - Product
struct Product: Codable {
    var productID: Int
    var productName: String?
    var productImage: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "productId"
        case productName = "productName"
        case productImage = "productImage"
    }
}

// MARK: - NotificationResponse
struct NotificationResponse: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [Notification]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct Notification: Codable {
    var orderID: Int?
    var title, orderNumber: String?
    var orderStatus: Int?
    var responseObjDescription, orderDate, orderNotificationDateTime, orderNotificationDate: String?
    var orderNotificationTime: String?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case title = "title"
        case orderNumber = "orderNumber"
        case orderStatus = "orderStatus"
        case responseObjDescription = "description"
        case orderDate = "orderDate"
        case orderNotificationDateTime = "orderNotificationDateTime"
        case orderNotificationDate = "orderNotificationDate"
        case orderNotificationTime = "orderNotificationTime"
    }
}

// MARK: - SearchResult
struct SearchResult: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [Cook]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct SearchResultElement: Codable {
    var restnCookID: Int?
    var cooknRESTName, cooknRESTDescription: String?
    var cooknRESTImageURL: String?
    var pictureID, rating, ratingCount: Int?
    var isLike: Bool?
    var restnCookAddress: String?
    var isStatusAvailable: Bool?
    var cousinItems: [CousinItem]?
    var distance, openTime, closeTime: String?
    
    enum CodingKeys: String, CodingKey {
        case restnCookID = "restnCookId"
        case cooknRESTName = "cooknRestName"
        case cooknRESTDescription = "cooknRestDescription"
        case cooknRESTImageURL = "cooknRestImageURL"
        case pictureID = "pictureId"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case isLike = "isLike"
        case restnCookAddress = "restnCookAddress"
        case isStatusAvailable = "isStatusAvailable"
        case cousinItems = "cousinItems"
        case distance = "distance"
        case openTime = "openTime"
        case closeTime = "closeTime"
    }
}

// MARK: - CousinItem
struct CousinItem: Codable {
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
}

// MARK: - AllCardResponse
struct AllCardResponse: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [Card]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - Card
struct Card: Codable {
    var id, object: String?
    var account, addressCity, addressCountry, addressLine1: String?
    var addressLine1Check, addressLine2, addressState, addressZip: String?
    var addressZipCheck, availablePayoutMethods: String?
    var brand, country: String?
    var currency: String?
    var customer, cvcCheck: String?
    var defaultForCurrency: Bool?
    var dynamicLast4: String?
    var expMonth, expYear: Int?
    var fingerprint, funding, last4: String?
    var metadata: Metadata?
    var name, recipient, threeDSecure, tokenizationMethod: String?
    var responseObjDescription, iin, issuer: String?
    
    var isSelected:Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id, object, account
        case addressCity = "address_city"
        case addressCountry = "address_country"
        case addressLine1 = "address_line1"
        case addressLine1Check = "address_line1_check"
        case addressLine2 = "address_line2"
        case addressState = "address_state"
        case addressZip = "address_zip"
        case addressZipCheck = "address_zip_check"
        case availablePayoutMethods = "available_payout_methods"
        case brand, country, currency, customer
        case cvcCheck = "cvc_check"
        case defaultForCurrency = "default_for_currency"
        case dynamicLast4 = "dynamic_last4"
        case expMonth = "expMonth"
        case expYear = "expYear"
        case fingerprint, funding, last4, metadata, name, recipient
        case threeDSecure = "three_d_secure"
        case tokenizationMethod = "tokenization_method"
        case responseObjDescription = "description"
        case iin, issuer
    }
}

// MARK: - Metadata
struct Metadata: Codable {
}

// MARK: - TopRatedCook
struct TopRatedCook: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [Cook]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "ErrorMejssageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
    
    //Updation
    mutating func updateCook(newV : [Cook]) {
        responseObj = newV
    }
}

// MARK: - ResponseObj
struct Cook: Codable {
    var restnCookID: Int?
    var cooknRESTName, cooknRESTDescription: String?
    var cooknRESTImageURL: String?
    var pictureID, ratingCount: Int?
    var rating: Double?
    var isLike: Bool?
    var restnCookAddress: String?
    var isStatusAvailable: Bool?
    var cousinItems: [CousinItemCook]?
    var distance, openTime, closeTime: String?
    
    enum CodingKeys: String, CodingKey {
        case restnCookID = "restnCookId"
        case cooknRESTName = "cooknRestName"
        case cooknRESTDescription = "cooknRestDescription"
        case cooknRESTImageURL = "cooknRestImageURL"
        case pictureID = "pictureId"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case isLike = "isLike"
        case restnCookAddress = "restnCookAddress"
        case isStatusAvailable = "isStatusAvailable"
        case cousinItems = "cousinItems"
        case distance = "distance"
        case openTime = "openTime"
        case closeTime = "closeTime"
    }
}

// MARK: - CousinItem
struct CousinItemCook: Codable {
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
}

// MARK: - TopRatedDishes
struct TopRatedDishes: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [Dish]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct Dish: Codable {
    var restnCookID: Int?
    var restnCook: String?
    var restnCookPictureID: Int?
    var restnCookPictureURL: String?
    var itemID, quantity: Int?
    var itemName, responseObjDescription: String?
    var available: Bool?
    var cuisine: String?
    var preferences: [Preference]?
    var imageURL: [ImageURL]?
    var price: String?
    var ratingCount, reviewCount: Int?
    var rating: Double?
    var customerImages: [CustomerImage]?
    var isAvailableAfterhrs: Bool?
    var availableAfterhrs: String?
    
    enum CodingKeys: String, CodingKey {
        case restnCookID = "restnCookId"
        case restnCook = "restnCook"
        case restnCookPictureID = "restnCookPictureId"
        case restnCookPictureURL = "restnCookPictureURL"
        case itemID = "itemId"
        case quantity = "quantity"
        case itemName = "itemName"
        case responseObjDescription = "description"
        case available = "available"
        case cuisine = "cuisine"
        case preferences = "preferences"
        case imageURL = "imageUrl"
        case price = "price"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case reviewCount = "reviewCount"
        case customerImages = "customerImages"
        case isAvailableAfterhrs = "isAvailableAfterhrs"
        case availableAfterhrs = "availableAfterhrs"
    }
}

// MARK: - CustomerImage
struct CustomerImage: Codable {
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case image = "image"
    }
}

// MARK: - ImageURL
struct ImageURL: Codable {
    var productImageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case productImageURL = "productImageURL"
    }
}

// MARK: - Preference
struct Preference: Codable {
    var name: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
    }
}

// MARK: - CookDetails
struct CookDetails: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [CookDetail]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct CookDetail: Codable {
    var restnCookID: Int?
    var cooknRESTName, cooknRESTDescription: String?
    var cooknRESTImageURL: String?
    var pictureID, ratingCount: Int?
    var rating: Float?
    var isLike: Bool?
    var restnCookAddress: String?
    var isStatusAvailable: Bool?
    var cousinItems: [CousinItem]?
    var distance, openTime, closeTime: String?
    
    enum CodingKeys: String, CodingKey {
        case restnCookID = "restnCookId"
        case cooknRESTName = "cooknRestName"
        case cooknRESTDescription = "cooknRestDescription"
        case cooknRESTImageURL = "cooknRestImageURL"
        case pictureID = "pictureId"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case isLike = "isLike"
        case restnCookAddress = "restnCookAddress"
        case isStatusAvailable = "isOpen"
        case cousinItems = "cousinItems"
        case distance = "distance"
        case openTime = "openTime"
        case closeTime = "closeTime"
    }
}

//// MARK: - CousinItem
//struct CousinItem: Codable {
//    var name: String?
//
//    enum CodingKeys: String, CodingKey {
//        case name = "Name"
//    }
//}

// MARK: - DishDetails
struct DishDetails: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [DishDetail]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - CookGallery
struct CookGallery: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: [CookGalleryResponseObj]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct CookGalleryResponseObj: Codable {
    let address: String?
    let imageID: Int?
    let imageCaption: String?
    let imageURL: String?
    let galleryType: Int?
    
    enum CodingKeys: String, CodingKey {
        case address = "address"
        case imageID = "imageId"
        case imageCaption = "imageCaption"
        case imageURL = "imageURL"
        case galleryType = "galleryType"
    }
}


// MARK: - ResponseObj
struct DishDetail: Codable {
    var id: Int?
    var name, responseObjDescription: String?
    var quantity: Int?
    var price: String?
    var ratingCount, reviewCount: Int?
    var rating: Double?
    var customerImages: [CustomerImage]?
    var comments: [Comment]?
    var preferences: [Preference]?
    var imageURL: [ImageURL]?
    var specifications: [Specification]?
    var cuisine, dishPrepareTime: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case responseObjDescription = "description"
        case quantity = "quantity"
        case price = "price"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case reviewCount = "reviewCount"
        case customerImages = "customerImages"
        case comments = "comments"
        case preferences = "preferences"
        case imageURL = "imageUrl"
        case specifications = "specifications"
        case cuisine = "cuisine"
        case dishPrepareTime = "dishPrepareTime"
    }
}

// MARK: - Comment
struct Comment: Codable {
    var customerName, comment: String?
    var customerImage: String?
    
    enum CodingKeys: String, CodingKey {
        case customerName = "customerName"
        case comment = "comment"
        case customerImage = "customerImage"
    }
}

//// MARK: - CustomerImage
//struct CustomerImage: Codable {
//    var image: String?
//
//    enum CodingKeys: String, CodingKey {
//        case image = "Image"
//    }
//}

//// MARK: - ImageURL
//struct ImageURL: Codable {
//    var productImageURL: String?
//
//    enum CodingKeys: String, CodingKey {
//        case productImageURL = "ProductImageURL"
//    }
//}

//// MARK: - Preference
//struct Preference: Codable {
//    var name: String?
//    var image: String?
//
//    enum CodingKeys: String, CodingKey {
//        case name = "Name"
//        case image = "Image"
//    }
//}

// MARK: - Specification
struct Specification: Codable {
    var name, value: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case value = "value"
    }
}

// MARK: - CookFilterValues
struct CookFilterValues: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: JSONNull?
    var status: Bool?
    var responseObj: [Values]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct Values: Codable {
    var titleID: Int?
    var titleName, titleType: String?
    var categoryValueFilter: [CategoryValueFilter]?
    
    enum CodingKeys: String, CodingKey {
        case titleID = "titleId"
        case titleName = "titleName"
        case titleType = "titleType"
        case categoryValueFilter = "categoryValueFilter"
    }
}

// MARK: - CategoryValueFilter
struct CategoryValueFilter: Codable, Equatable {
    var titleValueID: Int?
    var titleValueName: String?
    
    enum CodingKeys: String, CodingKey {
        case titleValueID = "titleValueId"
        case titleValueName = "titleValueName"
    }
}

// MARK: - CookProducts
struct CookProducts: Codable {
    var statusCode: Int?
    var errorMessageTitle, errorMessage: String?
    var status: Bool?
    var responseObj: [CookProduct]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct CookProduct: Codable {
    var categoryID: Int?
    var categoryName: String?
    var categoryItems: [CategoryItem]?
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "categoryId"
        case categoryName = "categoryName"
        case categoryItems = "categoryItems"
    }
}

// MARK: - CategoryItem
struct CategoryItem: Codable {
    var itemID: Int?
    var itemName: String?
    var quantity: Int?
    var categoryItemDescription: String?
    var available: Bool?
    var cuisine: String?
    var preferences: [Preference]?
    var imageURL: [ImageURL]?
    var price: String?
    var ratingCount, reviewCount: Int?
    var rating: Float?
    var customerImages: [CustomerImage]?
    var productProductAttributes: [ProductProductAttribute]?
    var isProductAttributesExist: Bool?
    
    enum CodingKeys: String, CodingKey {
        case itemID = "itemId"
        case itemName = "itemName"
        case quantity = "quantity"
        case categoryItemDescription = "description"
        case available = "available"
        case cuisine = "cuisine"
        case preferences = "preferences"
        case imageURL = "imageUrl"
        case price = "price"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case reviewCount = "reviewCount"
        case customerImages = "customerImages"
        case productProductAttributes = "productProductAttributes"
        case isProductAttributesExist = "isProductAttributesExist"
    }
}

// MARK: - ProductProductAttribute
struct ProductProductAttribute: Codable {
    var productAttributeMappingID, productAttributeID, productID: Int
    var attributeName: String?
    var attributeTypeID: Int?
    var attributeType: JSONNull?
    var isRequired: Bool?
    var productAttribute: [ProductAttribute]?
    
    enum CodingKeys: String, CodingKey {
        case productAttributeMappingID = "productAttributeMappingId"
        case productAttributeID = "productAttributeId"
        case productID = "productId"
        case attributeName = "attributeName"
        case attributeTypeID = "attributeTypeId"
        case attributeType = "attributeType"
        case isRequired = "isRequired"
        case productAttribute = "productAttribute"
    }
}

// MARK: - ProductAttribute
struct ProductAttribute: Codable {
    var productAttributeValueID: Int?
    var name: String?
    var price: String?
    var isPreSelected: Bool?
    var currency: String?
    var isSelected: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case productAttributeValueID = "productAttributeValueId"
        case name = "name"
        case price = "price"
        case isPreSelected = "isPreSelected"
        case currency = "currency"
    }
}

// MARK: - getAllPaymentMethods
struct AllPaymentMethods: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: [AllPaymentMethodsResponseObj]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - AllPaymentMethodsResponseObj
struct AllPaymentMethodsResponseObj: Codable {
    let paymentMethodId: Int?
    let paymentMethodName: String?
    let isShow: Bool?

    enum CodingKeys: String, CodingKey {
        case paymentMethodId = "paymentMethodId"
        case paymentMethodName = "paymentMethodName"
        case isShow = "isShow"
    }
}

// MARK: - AddToCart
struct AddToCart: Codable,DefaultStorable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: AddToCartResponseObj?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}


// MARK: - ResponseObj
struct AddToCartResponseObj: Codable {
    let subtotal: String?
    let currencySymbol: String?
    let deliveryCharges: String?
    let tip: String?
    let discountAmount: String?
    let tax: String?
    let discountCode: String?
    let discountID: Double?
    let total: String?
    let coupon: String?
    let minDateOrder: String?
    let cartProducts: [CartProduct]?
    
    enum CodingKeys: String, CodingKey {
        case subtotal = "subtotal"
        case currencySymbol = "currencySymbol"
        case tax = "tax"
        case deliveryCharges = "deliveryCharges"
        case tip = "tip"
        case discountAmount = "discountAmount"
        case discountCode = "discountCode"
        case discountID = "discountId"
        case total = "total"
        case coupon = "coupon"
        case minDateOrder = "minDateOrder"
        case cartProducts = "cartProducts"
    }
}

// MARK: - CartProduct
struct CartProduct: Codable {
    let cartItemID: Int?
    let productAttribute: String?
    let productAttributePrice: String?
    let productAttributeValue: String?
    let productID, quantity: Int
    let productName: String?
    let productPrice: String?
    let productImageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case cartItemID = "cartItemId"
        case productAttribute = "productAttribute"
        case productID = "productId"
        case quantity = "quantity"
        case productName = "productName"
        case productPrice = "productPrice"
        case productImageURL = "productImageURL"
        case productAttributePrice = "productAttributePrice"
        case productAttributeValue = "productAttributeValue"
    }
}


// MARK: - CartCount
struct CartCount: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: Int?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}


// MARK: - CheckoutCart
struct CheckoutCart: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: CheckoutResponseObj?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct CheckoutResponseObj: Codable {
    let orderID: Int?
    let orderNumber: String?
    let orderTotal: String?
    let orderTax, deliveryCharges, quantity: String?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case orderNumber = "orderNumber"
        case orderTotal = "orderTotal"
        case orderTax = "orderTax"
        case deliveryCharges = "deliveryCharges"
        case quantity = "quantity"
    }
}

// MARK: - TrackOrder
struct TrackOrder: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: JSONNull?
    let status: Bool?
    let responseObj: [TrackOrderResponseObj]?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct TrackOrderResponseObj: Codable {
    let orderID: Int?
    var orderDate, orderNumber, amount,subAmount, orderStatus, tax,discountAmount: String?
    let billingAddress, shippingAddress, cooknChefAddress,merchantAddressType, orderNotes, additionalInstructions: String?
    let orderedItems: [OrderedItem]?
    let orderedStatusArray: [OrderedStatusArray]?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case orderDate = "orderDate"
        case orderNumber = "orderNumber"
        case amount = "amount"
        case subAmount = "subAmount"
        case tax = "tax"
        case discountAmount = "discountAmount"
        case orderStatus = "orderStatus"
        case billingAddress = "billingAddress"
        case shippingAddress = "shippingAddress"
        case merchantAddressType = "merchantAddressType"
        case cooknChefAddress = "cooknChefAddress"
        case orderedItems = "orderedItems"
        case orderedStatusArray = "orderedStatusArray"
        case orderNotes = "orderNotes"
        case additionalInstructions = "additionalInstructions"
    }
}

// MARK: - OrderedItem
struct OrderedItem: Codable {
    let productName: String?
    let productQuantity: Int?
    var productTotalPrice, productPrice: String?
    let productImage: String?
    
    enum CodingKeys: String, CodingKey {
        case productName = "productName"
        case productQuantity = "productQuantity"
        case productTotalPrice = "productTotalPrice"
        case productPrice = "productPrice"
        case productImage = "productImage"
    }
}

// MARK: - OrderedStatusArray
struct OrderedStatusArray: Codable {
    let statusID: Int?
    let statusTitle, statusTime, statusDescription, statusDate: String?
    let isdone: Bool?
    
    enum CodingKeys: String, CodingKey {
        case statusID = "statusId"
        case statusTitle = "statusTitle"
        case statusTime = "statusTime"
        case statusDescription = "statusDescription"
        case statusDate = "statusDate"
        case isdone = "isdone"
    }
}

// MARK: - PaymentCheckout
struct PaymentCheckout: Codable {
    let statusCode: Int?
    let errorMessageTitle: String?
    let errorMessage: String?
    let status: Bool?
    let responseObj: PaymentCheckoutResponseObj?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct PaymentCheckoutResponseObj: Codable {
    let orderNumber, orderGUID, orderArrivalTime: String?
    
    enum CodingKeys: String, CodingKey {
        case orderNumber = "orderNumber"
        case orderGUID = "orderGuid"
        case orderArrivalTime = "orderArrivalTime"
    }
}

struct GetRestaurantsByType: Codable {
   // let statusCode: Int?
    let errorMessage: String?
   //let errorMessageTitle: String?
    let status: Bool?
    let responseObj: [Restaurant]?
    
    enum CodingKeys: String, CodingKey {
        case errorMessage = "errorMessage"
       // case errorMessageTitle = "errorMessageTitle"
        case status = "status"
       // case statusCode = "statusCode"
        case responseObj = "responseObj"
    }
}

struct Restaurant: Codable {
    let restnCookId: Int?
    let cooknRestName: String?
    let cuisine: String?
    let cooknRestImageURL: String?
    let pictureId: Int?
    let rating: Double?
    let ratingCount: Int?
    let restnCookAddress: String?
    let isOpen: Bool?
    let distance: String?
    let openTime: String?
    let closeTime: String?
    let cutofftime: String?
    let isRestAvailable: Bool?
    let openCloseTime: [String]?

    enum CodingKeys: String, CodingKey {
        case restnCookId = "restnCookId"
        case cooknRestName = "cooknRestName"
        case cuisine = "cuisine"
        case cooknRestImageURL = "cooknRestImageURL"
        case pictureId = "pictureId"
        case rating = "rating"
        case ratingCount = "ratingCount"
        case restnCookAddress = "restnCookAddress"
        case isOpen = "isOpen"
        case distance = "distance"
        case openTime = "openTime"
        case closeTime = "closeTime"
        case isRestAvailable = "isRestAvailable"
        case cutofftime = "cutofftime"
        case openCloseTime = "openCloseTime"
    }
}

// MARK: - getStoreOptions

struct GetStoreOptions: Codable {
    let errorMessage: String?
    let status: Bool?
    let responseObj: [GetStoreOptionsResponseObj]?
    
    enum CodingKeys: String, CodingKey {
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

struct GetStoreOptionsResponseObj: Codable {
    let id: Int?
    let name: String?
    let isAvailable: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case isAvailable = "isAvailable"
    }
}
// MARK: - OnGoIngOrder
struct OnGoIngOrder: Codable {
    let statusCode: Int?
    let errorMessageTitle, errorMessage: String?
    let status: Bool?
    let responseObj: OnGoIngOrderDetails?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case errorMessageTitle = "errorMessageTitle"
        case errorMessage = "errorMessage"
        case status = "status"
        case responseObj = "responseObj"
    }
}

// MARK: - ResponseObj
struct OnGoIngOrderDetails : Codable {
    let delivery: [OnGoIngOrderResponseObj]?
    let takeaway: [OnGoIngOrderResponseObj]?
    
    enum CodingKeys: String, CodingKey {
        case delivery = "delivery"
        case takeaway = "takeaway"
    }
}

struct OnGoIngOrderResponseObj : Codable {
    let orderID: String?
    let status: String?
    let deliveredTime, orderNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case status = "status"
        case deliveredTime = "deliveredTime"
        case orderNumber = "orderNumber"
    }
}



// MARK: - Encode/decode helpers
class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        var container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    var key: String
    
    required init?(intValue: Int) {
        return nil
    }
    
    required init?(stringValue: String) {
        key = stringValue
    }
    
    var intValue: Int? {
        return nil
    }
    
    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {
    
    var value: Any
    
    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        var context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }
    
    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        var context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }
    
    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if var value = try? container.decode(Bool.self) {
            return value
        }
        if var value = try? container.decode(Int64.self) {
            return value
        }
        if var value = try? container.decode(Double.self) {
            return value
        }
        if var value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if var value = try? container.decode(Bool.self) {
            return value
        }
        if var value = try? container.decode(Int64.self) {
            return value
        }
        if var value = try? container.decode(Double.self) {
            return value
        }
        if var value = try? container.decode(String.self) {
            return value
        }
        if var value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if var value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if var value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if var value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if var value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if var value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            var value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }
    
    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            var value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }
    
    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if var value = value as? Bool {
                try container.encode(value)
            } else if var value = value as? Int64 {
                try container.encode(value)
            } else if var value = value as? Double {
                try container.encode(value)
            } else if var value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if var value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if var value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            var key = JSONCodingKey(stringValue: key)!
            if var value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if var value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if var value = value as? Double {
                try container.encode(value, forKey: key)
            } else if var value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if var value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if var value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if var value = value as? Bool {
            try container.encode(value)
        } else if var value = value as? Int64 {
            try container.encode(value)
        } else if var value = value as? Double {
            try container.encode(value)
        } else if var value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }
    
    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            var container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        if var arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if var dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
