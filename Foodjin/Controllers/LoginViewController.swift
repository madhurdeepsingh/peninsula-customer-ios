//
//  ViewController.swift
//  Foodjin
//
//  Created by Navpreet Singh on 18/05/19.
//  Copyright © 2019 Foodjin. All rights reserved.
//

import UIKit
import UnderLineTextField
import Firebase
import CountryPicker

class LoginViewController: UIViewController {
    
    //Input Fields Outlets
    @IBOutlet weak var usernameTextField: UnderLineTextField!
    @IBOutlet weak var countryCodeTextField: UnderLineTextField!
    @IBOutlet weak var phoneNumberTextField: UnderLineTextField!
    @IBOutlet weak var passwordTextField: UnderLineTextField!
    @IBOutlet weak var passwordTextField2: UnderLineTextField!

    @IBOutlet weak var secondViewLeading: NSLayoutConstraint!
    //Buttons Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginViaButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerNowButton: UIButton!
    var picker: CountryPicker!
    var textLoginViaEmail = "Login using email"
    var textLoginViaPhone = "Login using phone number"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // view SetUp
        self.view.backgroundColor = APP_COLORS.backgroundColor
        self.signInButton.backgroundColor = APP_COLORS.buttonColor
        self.loginViaButton.tintColor = APP_COLORS.buttonColor
        self.forgotPasswordButton.tintColor = APP_COLORS.translucentButtonColor
        
        // Do any additional setup after loading the view.
        #if DEBUG
            self.usernameTextField.text = "navpreetsingh0790@gmail.com"
            self.passwordTextField.text = "Jaat@1234"
        
//        self.usernameTextField.text = "8283929290" //"7009928796"
//        self.passwordTextField.text = "Admin@1234" //"ARun!@34"

        self.usernameTextField.text = "91 7009928796"
        self.phoneNumberTextField.text = "9717564049"//"9876104311"
        self.countryCodeTextField.text = "91"
        self.passwordTextField.text = "Test@1234"
        self.passwordTextField2.text = "Test@123"
//        self.usernameTextField.text = "abhishek1612@yopmail.com"
//        self.passwordTextField.text = "Demo@1234"
        
        #endif
        picker = CountryPicker()
        picker.displayOnlyCountriesWithCodes = CountryCodes.codes
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .white, rowBackgroundColor: .black, showFlagsBorder: false)
        picker.theme = theme
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        self.countryCodeTextField.inputView = picker

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if loginViaButton.titleLabel?.text == self.textLoginViaPhone {
            self.scrollView.contentOffset.x = 0
        } else if loginViaButton.titleLabel?.text == self.textLoginViaEmail {
            self.scrollView.contentOffset.x = self.scrollView.frame.size.width
        }
//        setLoginViaButtonTitle()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        secondViewLeading.constant = scrollView.frame.size.width + 20
        print("ViewDIdAppear - ", secondViewLeading.constant)
        print("self.scrollView.contentOffset.x - ", self.scrollView.contentOffset.x)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

// MARK: IBActions
extension LoginViewController {
    
    @IBAction func ForgotPasswordAction(_ sender: Any) {
        
        guard let forgetPasswordVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController else {return}
        self.navigationController?.pushViewController(forgetPasswordVC, animated: true)
    }
    
    
    @IBAction func SignInAction(_ sender: Any) {
        
//        #if DEBUG
//        guard let verificationVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "VerficationViewController") as? VerficationViewController else {return}
//        verificationVC.isFromForgotPassword = false
//        self.navigationController?.pushViewController(verificationVC, animated: true)
//        #else
            self.validate()
//        #endif
    }
    
    @IBAction func RegisterNowAction(_ sender: Any) {
        guard let registerVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {return}
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func loginViaClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            if self.scrollView.contentOffset.x == 0 {
                sender.setTitle("Login using email", for: .normal)
                self.scrollView.contentOffset.x = self.scrollView.frame.size.width
            }
            else {
                sender.setTitle("Loging using phone number", for: .normal)
                self.scrollView.contentOffset.x = 0
            }
            print("button self.scrollView.contentOffset.x - ", self.scrollView.contentOffset.x)
        }
        
    }
    
    func setLoginViaButtonTitle() {
        
        UIView.animate(withDuration: 0.3) {
            if self.scrollView.contentOffset.x == 0 {
                self.loginViaButton.setTitle(self.textLoginViaEmail, for: .normal)
                self.scrollView.contentOffset.x = self.scrollView.frame.size.width
            }
            else {
                self.loginViaButton.setTitle(self.textLoginViaPhone, for: .normal)
                self.scrollView.contentOffset.x = 0
            }
        }
    }
}

extension LoginViewController: CountryPickerDelegate {
    // a picker item was selected
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
//        print(phoneCode)
        self.countryCodeTextField.text = phoneCode
    }
}

extension LoginViewController {
    func validate() {
        if scrollView.contentOffset.x == 0 {
            if (self.usernameTextField.text?.count ?? 0 <= 0) && (self.passwordTextField.text?.count ?? 0 <= 0 ) {
                self.showAlert(for: "Please enter your Email & Password")
                return
            }
            
            do {
                _ = try usernameTextField.validatedText(validationType: ValidatorType.requiredField(field: "Username"))
                _ = try passwordTextField.validatedText(validationType: ValidatorType.requiredField(field: "Password"))
                
                _ = try usernameTextField.validatedText(validationType: ValidatorType.email)
                _ = try passwordTextField.validatedText(validationType: ValidatorType.password)
                
                self.loginIntoApplication()
            } catch(let error) {
                showAlert(for: (error as! ValidationError).message)
            }
        }
        else {
            if (self.phoneNumberTextField.text?.count ?? 0 <= 0) && (self.passwordTextField2.text?.count ?? 0 <= 0 ) && (self.countryCodeTextField.text?.count ?? 0 <= 0) {
                self.showAlert(for: "Please enter your Country code, Phone Number & Password")
                return
            }
            
            do {
                _ = try phoneNumberTextField.validatedText(validationType: ValidatorType.requiredField(field: "Phone Number"))
                _ = try passwordTextField2.validatedText(validationType: ValidatorType.requiredField(field: "Password"))
                
                //_ = try phoneNumberTextField.validatedText(validationType: ValidatorType.)
                _ = try passwordTextField2.validatedText(validationType: ValidatorType.password)
                
                self.loginIntoApplication(isphone: true)
            } catch(let error) {
                showAlert(for: (error as! ValidationError).message)
            }
        }
    }
    
    func loginIntoApplication(isphone: Bool = false) {
        
        InstanceID.instanceID().instanceID { (result, error) in
            print("Token:: \(result?.token)")
            if error == nil {
                let fcmDeviceToken = result!.token
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                var email = ""
                var phone = ""
                var psword = ""
                
                //                do {
                //                    if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: self.usernameTextField.text ?? "", options: [], range: NSRange(location: 0, length: self.usernameTextField.text?.count ?? 0)) != nil {
                //
                //                        email = self.usernameTextField.text ?? ""
                //
                //                    } else {
                //                        phone = (self.countryCodeTextField.text ?? "") + " " + (self.phoneNumberTextField.text ?? "")
                //                    }
                //                } catch(let error) {
                //                    //            showAlert(for: (error as! ValidationError).message)
                //                }
                if !isphone {
                    email = self.usernameTextField.text ?? ""
                    psword = self.passwordTextField.text ?? ""
                    
                } else {
                    phone = (self.countryCodeTextField.text ?? "") + " " + (self.phoneNumberTextField.text ?? "")
                    phone = phone.replacingOccurrences(of: "+", with: "").replacingOccurrences(of: "-", with: "")
                    psword = self.passwordTextField2.text ?? ""
                }
                
                let params: [String:Any] = [
                    "Email":email,
                    "Pswd":psword,
                    "Phone":phone,
                    "Imei1":"",
                    "Imei2":"",
                    "DeviceNo":"",
                    "LatPos":"\(appDelegate.latitudeLabel)" ?? "",
                    "LongPos":"\(appDelegate.longitudeLabel)" ?? "",
                    "ApiKey":API_KEY,
                    "DeviceToken":fcmDeviceToken,
                    "StoreId":STORE_ID ]
                
                Loader.show(animated: true)
                
                WebServiceManager.instance.post(url: Urls.login, params: params, successCompletionHandler: { [unowned self] (jsonData) in
                    Loader.hide()
                    let loginDict = try? JSONDecoder().decode(Register.self, from: jsonData)
                    print(loginDict)
                    
                    if loginDict?.responseObj?.userStatus == "InActive" {
                        print("Navigate to otp Screen with user Id")
                        guard let verificationVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "VerficationViewController") as? VerficationViewController else {return}
                        verificationVC.customerId = loginDict?.responseObj?.userID as? Int
                        verificationVC.isResend = true
                        self.navigationController?.pushViewController(verificationVC, animated: true)
                    } else {
                        //land to main screen
                        let uId = loginDict?.responseObj?.userID ?? 0
                        UserDefaults.standard.set(uId, forKey: userId)
                        
                        guard let landingVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "LandingTabbarController") as? LandingTabbarController else {return}
                        landingVC.selectedIndex = 1
                        //refrence to tab bar beavuse cart button not working on login
                        appDelegate.tabbarController = landingVC
                        self.navigationController?.pushViewController(landingVC, animated: false)
                    }
                    
                }) { (ResponseStatus) in
                    Loader.hide()
                    self.showAlertWithTitle(title: ResponseStatus.errorMessageTitle ?? "", msg: ResponseStatus.errorMessage ?? "")
                }
            }
            
        }
        
    }
    
}
