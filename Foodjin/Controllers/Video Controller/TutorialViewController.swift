//
//  TutorialViewController.swift
//  Aeropuerto
//
//  Created by Abhishek Sharma on 06/01/20.
//  Copyright © 2020 Foodjin. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstHeight: NSLayoutConstraint!
    @IBOutlet weak var firstWidth: NSLayoutConstraint!
    @IBOutlet weak var secondHeight: NSLayoutConstraint!
    @IBOutlet weak var secondWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdHeight: NSLayoutConstraint!
    @IBOutlet weak var thirdWidth: NSLayoutConstraint!
    
    var completedBlock:(()-> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstHeight.constant = scrollView.frame.size.height
        secondHeight.constant = scrollView.frame.size.height
        thirdHeight.constant = scrollView.frame.size.height
        
        firstWidth.constant = scrollView.frame.size.width
        secondWidth.constant = scrollView.frame.size.width
        thirdWidth.constant = scrollView.frame.size.width
        
       //scrollView.contentSize = CGSize(width: scrollView.frame.size.width * 3.0, height: scrollView.frame.size.height)
//        print("contentSize:: \(scrollView.contentSize)")
    }
    
    @IBAction func skipClicked(sender: UIButton) {
        completedBlock?()
    }
    
    @IBAction func nextClicked(sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            var offset = self.scrollView.contentOffset
            offset.x = self.scrollView.contentOffset.x + self.scrollView.frame.width
            self.scrollView.contentOffset = offset
        }
       
        print("new offset:: \(self.scrollView.contentOffset.x)")
    }
    
    @IBAction func getStartedClicked(sender: UIButton) {
        completedBlock?()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
