//
//  ForgotPasswordViewController.swift
//  Foodjin
//
//  Created by Navpreet Singh on 19/05/19.
//  Copyright © 2019 Foodjin. All rights reserved.
//

import UIKit
import UnderLineTextField
import CountryPicker

class ForgotPasswordViewController: UIViewController,CountryPickerDelegate {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailTextField: UnderLineTextField!
    @IBOutlet weak var recoverUsingPhone: UIButton!
    @IBOutlet weak var phoneTextField: UnderLineTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var secondViewLeading: NSLayoutConstraint!
    @IBOutlet weak var countryCodeTextField: UnderLineTextField!
    var picker: CountryPicker!
    var textUsingEmail = "Recover using email"
    var textUsingPhone = "Recover using phone number"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        #if DEBUG
            self.emailTextField.text = "navpreetsingh0790@gmail.com"
        #endif
        picker = CountryPicker()
        picker.displayOnlyCountriesWithCodes = CountryCodes.codes
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .white, rowBackgroundColor: .black, showFlagsBorder: false)
        picker.theme = theme
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        self.countryCodeTextField.inputView = picker
//        self.automaticallyAdjustsScrollViewInsets = false
        self.scrollView.contentInsetAdjustmentBehavior = .never
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        secondViewLeading.constant = scrollView.frame.width + 10
        if recoverUsingPhone.titleLabel?.text == self.textUsingPhone {
            descriptionLabel.text = "We just need your registered email id to send you password reset link"
            self.scrollView.contentOffset.x = 0
        } else if recoverUsingPhone.titleLabel?.text == self.textUsingEmail {
            descriptionLabel.text = "We just need your registered phone number to send you password reset link"
            self.scrollView.contentOffset.x = self.scrollView.frame.size.width
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
    //        print(phoneCode)
            self.countryCodeTextField.text = phoneCode
        }
}



extension ForgotPasswordViewController {
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func usePhoneToRecover(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            if self.scrollView.contentOffset.x == 0 {
                self.scrollView.contentOffset.x = self.scrollView.frame.size.width
                sender.setTitle(self.textUsingEmail, for: .normal)
                self.descriptionLabel.text = "We just need your registered phone number to send you password reset link"
            }
            else {
                self.scrollView.contentOffset.x = 0
                sender.setTitle(self.textUsingPhone, for: .normal)
                self.descriptionLabel.text = "We just need your registered email id to send you password reset link"
            }
        }
        
    }
    
    
    @IBAction func sendLinkAction(_ sender: Any) {
        self.validate()
    }
    
    func validate() {
        if scrollView.contentOffset.x == 0 {
            do {
                _ = try emailTextField.validatedText(validationType: ValidatorType.requiredField(field: "Email"))
                //            _ = try emailTextField.validatedText(validationType: ValidatorType.email)
                
                self.forgotPasswordApi()
            } catch(let error) {
                showAlert(for: (error as! ValidationError).message)
            }
        }
        else {
            do {
                _ = try phoneTextField.validatedText(validationType: ValidatorType.requiredField(field: "Phone Number"))
                _ = try countryCodeTextField.validatedText(validationType: ValidatorType.requiredField(field: "Country Code"))
                //            _ = try emailTextField.validatedText(validationType: ValidatorType.email)
                
                self.forgotPasswordApi(isphone: true)
            } catch(let error) {
                showAlert(for: (error as! ValidationError).message)
            }
        }
    }
    
    func forgotPasswordApi(isphone:Bool = false) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        var email = ""
        var phone = ""
        
        do {
            if !isphone {
                
//                 _ = try emailTextField.validatedText(validationType: ValidatorType.email)
                
                email = self.emailTextField.text ?? ""
                
            } else {
                phone = (self.countryCodeTextField.text ?? "") + " " + (self.phoneTextField.text ?? "")
                phone = phone.replacingOccurrences(of: "+", with: "").replacingOccurrences(of: "-", with: "")
            }
        } catch(let error) {
//            showAlert(for: (error as! ValidationError).message)
        }
    

        
        let params: [String:Any] = [
            "Email":email,
            "Phone":phone,
            "Imei1":"",
            "Imei2":"",
            "DeviceNo":"",
            "LatPos":"\(appDelegate.latitudeLabel)",
            "LongPos":"\(appDelegate.longitudeLabel)",
            "ApiKey":API_KEY,
            "DeviceToken":"",
            "StoreId":STORE_ID ]
        
        Loader.show(animated: true)
        
        WebServiceManager.instance.post(url: Urls.forgotPasswword, params: params, successCompletionHandler: { [unowned self] (jsonData) in
            Loader.hide()
            let loginDict = try? JSONDecoder().decode(Register.self, from: jsonData)
            print(loginDict)
            
            //get Userid and Otp navigate to Otp Screen
            guard let verificationVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "VerficationViewController") as? VerficationViewController else {return}
            verificationVC.isFromForgotPassword = true
            verificationVC.customerId = loginDict?.responseObj?.userID as? Int
            self.navigationController?.pushViewController(verificationVC, animated: true)
//            self.showAlertWithTitle(title: "Alert", msg: "OTP sent successfully to your registered mobile number")

        }) { (ResponseStatus) in
            Loader.hide()
            self.showAlertWithTitle(title: ResponseStatus.errorMessageTitle ?? "", msg: ResponseStatus.errorMessage ?? "")
        }
    }
    
    func navigateTOCreatePassword() {
        guard let createPasswordVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: "CreatePasswordViewController") as? CreatePasswordViewController else {return}
        self.navigationController?.pushViewController(createPasswordVC, animated: true)
    }
}
