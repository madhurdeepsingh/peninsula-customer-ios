

//
//  AddAttributeViewController.swift
//  Aeropuerto
//
//  Created by Abhishek Sharma on 30/12/19.
//  Copyright © 2019 Foodjin. All rights reserved.
//

import UIKit
import BEMCheckBox


class AttributeCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var imageVw:BEMCheckBox!

}

class AddAttributeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var categoryItem: CategoryItem?
    var successBlock:((CategoryItem) -> Void)?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoryItem?.productProductAttributes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categoryItem!.productProductAttributes![section].productAttribute?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AttributeCell") as? AttributeCell {
            let item = categoryItem!.productProductAttributes![indexPath.section].productAttribute![indexPath.row]
            cell.titleLbl.text = item.name
            cell.subTitleLbl.text = "\(item.currency ?? "")\(item.price ?? "")"
            print("section \(indexPath.section), value - \(item.name!), attr id - \(categoryItem!.productProductAttributes![indexPath.section].attributeTypeID)")
            if (categoryItem!.productProductAttributes![indexPath.section].attributeTypeID ?? 0) == 1 || (categoryItem!.productProductAttributes![indexPath.section].attributeTypeID ?? 0) == 2 {
                cell.imageVw.boxType = .square
                cell.imageVw.setOn(item.isSelected, animated: false)
//                print("section \(indexPath.section), value - \(item.name!), attr id - \(categoryItem!.productProductAttributes![indexPath.section].attributeTypeID)")
            } else if (categoryItem!.productProductAttributes![indexPath.section].attributeTypeID ?? 0) == 3 {
                cell.imageVw.boxType = .circle
                cell.imageVw.setOn(item.isSelected, animated: false)
//                print("section \(indexPath.section), value - \(item.name!), attr id - \(categoryItem!.productProductAttributes![indexPath.section].attributeTypeID)")
            }
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categoryItem!.productProductAttributes![section].attributeName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let superItem = categoryItem!.productProductAttributes![indexPath.section]
        let item = categoryItem!.productProductAttributes![indexPath.section].productAttribute![indexPath.row]
        if (superItem.attributeTypeID ?? 0) == 1  || (superItem.attributeTypeID ?? 0) == 2  {
            categoryItem!.productProductAttributes![indexPath.section].productAttribute![indexPath.row].isSelected = !item.isSelected
        }
        else if (superItem.attributeTypeID ?? 0) == 3 {
            for index in 0..<superItem.productAttribute!.count {
                categoryItem!.productProductAttributes![indexPath.section].productAttribute![index].isSelected = false
            }
            categoryItem!.productProductAttributes![indexPath.section].productAttribute![indexPath.row].isSelected = !item.isSelected

        }
        
        tableView.reloadRows(at: tableView.indexPathsForVisibleRows!, with: .automatic)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(sender: UIButton) {
        successBlock?(categoryItem!)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
